/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 27.07.2015
  * @author Michael Lange
  */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
 
public class LogikSimulator {

  public int btID = 0;
  
  // Anfang Attribute
  final private SimulationPanel simulationPanel;                                //Das eigentliche Simulatorfenster
  final private JScrollPane view;                                               //wird eingebettet in ein Fenster mit Scrollbalken
  final private JPanel buttonPanel;                                             //Das Buttonpanel nimmt Buttons auf, die das Hinzufgen und Lschen von Bauteilen,
  final private JButton clearButton,                                            //deren Verkabelung bzw das Kappen der Verkabelung sowie das Lschen des                                          //aufegfhrt werden. Sie knnten auch zur Laufzeit erstellt werden.
                        andButton,
                        orButton,
                        notButton,
                        nandButton,
                        norButton,
                        xorButton,
                        xnorButton,
                        deleteButton,
                        tasterButton,
                        ledButton;
  private final ActionListener actionHandler;                                   //Der ActionListener horcht auf das Drcken der oben angegebenen Buttons und
                                                                                //lst damit die entsprechenden Interaktionen aus.
  // Ende Attribute
  
  public LogikSimulator() {
    JFrame frame = new JFrame("Logik-Simulator");
    
    setLookandFeel();

    
    actionHandler = new ActionListener() {
      
      @Override
      public void actionPerformed(final ActionEvent e) {
        String s = e.getActionCommand();
        
        switch (s) {                                                            //Je nachdem, welcher Button gedrckt wurde, soll eine andere Aktion
          case "Clear":                                                         //ausgefhrt werden. Ich unterscheide sie, nach dem Buttontext.
          simulationPanel.removeAll();                                          //Wird etwa der Button "Entferne Bauteil" gedrckt, so wird das ActionCommand
          simulationPanel.clearPaint();                                         //im ActionEvent e genau diesen Text enthalten.
          simulationPanel.repaint();                                            //Die switch-Anweisung verzweigt entsprechend.
          btID = 0;
          break;
          case "NAND":
          newNAND();
          break;
          case "OR":
          newOR(); 
          break;
          case "NOR":
          newNOR(); 
          break;
          case "NOT":
          newNOT(); 
          break;
          case "AND":
          newAND(); 
          break;
          case "XOR":
          newXOR(); 
          break;
          case "XNOR":
          newXNOR(); 
          break;
          case "Button":
          newBUTTON();
          break;
          case "LED":
          newLED(); 
          break;
          case "Delete":
          newDelete(); 
          break;
        } // end of switch
      }
    };
    
    buttonPanel = new JPanel(new GridLayout(2, 4, 10, 10));                       //Buttonleiste einrichten
    
    andButton = addButton("AND");                                               //und Buttons zufgen
    nandButton = addButton("NAND");
    orButton = addButton("OR");
    norButton = addButton("NOR");
    notButton = addButton("NOT");
    xorButton = addButton("XOR");
    xnorButton = addButton("XNOR");    
    tasterButton = addButton("Button");
    ledButton = addButton("LED");
    deleteButton = addButton("Delete");
    clearButton = addButton("Clear");
    
    simulationPanel = new SimulationPanel();                                    //eigentliches Simulatorfeld einrichten
    
    view = new JScrollPane (simulationPanel,
    ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);   //Scrollumgebung fr das Simulatorfeld
    
    GridBagLayout gbl = new GridBagLayout();                                    //Layout fr das Hauptfenster mit Simulatorfeld und Buttonleiste
    frame.getContentPane().setLayout( gbl );
    
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridx = 0; gbc.gridy = 0;
    gbc.gridwidth = 1; gbc.gridheight = 1;
    gbc.weightx = 1.0; gbc.weighty = 0;
    gbl.setConstraints( buttonPanel, gbc );
    frame.getContentPane().add( buttonPanel );
    
    GridBagConstraints gbd = new GridBagConstraints();
    gbd.fill = GridBagConstraints.BOTH;
    gbd.gridx = 0; gbd.gridy = 1;
    gbd.gridwidth = GridBagConstraints.REMAINDER;
    gbd.gridheight = GridBagConstraints.REMAINDER;
    gbd.weightx = 1.0; gbd.weighty = 1.0;
    gbl.setConstraints( view, gbd );
    frame.getContentPane().add( view );
    
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    frame.setSize(1024, 700);
    
    Timer timer = new Timer(750, new ActionListener() {                         //Danke fr den Tipp mitdem Timer. Ich hatte damit zu kmpfen.
      public void actionPerformed(ActionEvent e) {
        simulationPanel.updateImage();
      }
    });
  }
  // Anfang Methoden
  
  private JButton addButton(String s) {                                         //Fr jeden JButton wird viel gleicher Code ausfeghrt, um ihn im Buttonpanel
    JButton button = new JButton();                                             //aufzunehmen (Hintergrund setzen, Text setzen, ActionListener hinzufgen),
    button.setBackground(new Color(230, 240, 250));                             //sodass dafr eine eigene, nicht ffentliche Methode sinnvoll wurde.
    button.setBorder(BorderFactory.createEtchedBorder());
    button.setText(s);
    buttonPanel.add(button);
    button.addActionListener(actionHandler);
    return (button);
  }
  
  public static void main(String[] args) {                                //Das Hauptprogramm, bei dem im Wesentlichen die Methode run() berschrieben
    Runnable gui = new Runnable() {                                             //wurde.
      
      @Override
      public void run() {
        LogikSimulator logikSimulator = new LogikSimulator();
      }
    };
    SwingUtilities.invokeLater(gui);
  }
  
  private void newNAND(){                                                       //Standard fr eine Methode, die dem Simulatorfenster ein weiteres Bauteil
    NAND nand = new NAND(updateID());
    simulationPanel.add(nand);
  }
  
  private void newBUTTON(){
    BUTTON taster = new BUTTON(updateID());
    simulationPanel.add(taster);
  }
  
  private void newLED(){
     LED led = new LED(updateID());
    simulationPanel.add(led);    
  }
  
  private void newNOT(){
    NOT not = new NOT(updateID());
    simulationPanel.add(not);
  }
  
  private void newAND(){
    AND and = new AND(updateID());
    simulationPanel.add(and);
  }
  
  private void newOR(){
    OR or = new OR(updateID());
    simulationPanel.add(or);
  }
  
  private void newNOR(){
    NOR nor = new NOR(updateID());
    simulationPanel.add(nor);
  }
  private void newXOR(){
    XOR xor = new XOR(updateID());
    simulationPanel.add(xor);
  }
  private void newXNOR(){
    XNOR xnor = new XNOR(updateID());
    simulationPanel.add(xnor);
  }

  private void newDelete(){
    simulationPanel.deleteBtn();
  }
  
  public int updateID(){
    this.btID++;
    return this.btID;
  }

  private void setLookandFeel(){
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception e) {

    }
  }  
  // Ende Methoden
} // end of LogikSimulator

