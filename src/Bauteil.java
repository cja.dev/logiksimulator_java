/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
// import java.awt.geom.Line2D;
import java.util.*;

public abstract class Bauteil extends JButton {
    
    public int draggedX, draggedY, cnctX, cnctY;
    boolean state = false;

    //! inp, out arrays
    public ArrayList<Bauteil> inp = new ArrayList<Bauteil>();
    public ArrayList<Bauteil> out = new ArrayList<Bauteil>();

    public int id;
    public int btnX = 100;
    public int btnY = 100;
    public int btnHeight = 30;
    public int btnWidtht = 100;
    public int btnShift = 50;

    
    Bauteil(){
        super();
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));

        addMouseListener(new MouseAdapter(){
            public void mousePressed(MouseEvent e){
                if (e.getButton() == MouseEvent.BUTTON1){   // left button pressed
                    draggedX = (int) e.getX();
                    draggedY = (int) e.getY();
                }
            }
            public void mouseClicked(MouseEvent e){
                Bauteil bt = (Bauteil) e.getComponent();
                SimulationPanel simuPa = (SimulationPanel) bt.getParent();
                //! connect gates
                if (e.getButton() == MouseEvent.BUTTON3){   // right button -> connect buttons
                    Bauteil conctBt1 = (Bauteil) simuPa.conctBt[0];
                    Bauteil conctBt2 = (Bauteil) simuPa.conctBt[1];
                    // System.out.println("S1" + conctBt1);
                    // System.out.println("S2" + conctBt2);
                    if(conctBt1 == null && conctBt2 == null){ // wenn liste schon voll => setze [0] = bt; leere [1]
                        simuPa.conctBt[0] = bt;
                        simuPa.conctBt[1] = null;
                        // System.out.println("1: " + bt.getLabel());
                        setBorder(BorderFactory.createLineBorder(Color.BLUE, 5));
                    }else if(conctBt1 != null && conctBt2 == null){ // wenn nur [1] ist leer -> setze auf [1] => updateState()
                        simuPa.conctBt[1] = bt;
                        // System.out.println("2: " + bt.getLabel());
                        updateConct();
                        simuPa.conctBt[0] = null;
                        simuPa.conctBt[1] = null;
                    }
                }else{  // if left or middle button -> toggle state
                    toggleState();
                }
            }
        });
        addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseDragged(MouseEvent e){
                int modif = e.getModifiers();
                if((modif & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK){
                    setLocation(e.getX() - draggedX + getLocation().x,
                                e.getY() - draggedY + getLocation().y);
                }
            }
        });
        updateColor();    
    }

    public void updateColor(){
        if(state) this.setBorder(BorderFactory.createLineBorder(Color.GREEN, 5));
        else this.setBorder(BorderFactory.createLineBorder(Color.RED, 5));

        System.out.println("ipdate color: " + id );
    }
    public void toggleState(){
        this.state = !this.state;
        this.updateState();
    }
    public void updateConct(){
        SimulationPanel simuPa = (SimulationPanel) this.getParent();
        Bauteil btA = simuPa.conctBt[0];
        Bauteil btB = simuPa.conctBt[1];
        int btA_in_btB = 0;     //* count hwo often connection already exist (to prevent unneccessary connections)
        int btB_in_btA = 0;
        for(Bauteil i : btB.inp){               // count how often is btA/btB with btB/btA connected
            if(i.id == btA.id) btA_in_btB++;
        }
        for(Bauteil i : btA.out){
            if(i.id == btB.id) btB_in_btA++;
        }
        if(btA_in_btB >= 1 && btB_in_btA >= 1){ //* if already connected -> delete connection
            btA.out.remove(btB);
            btB.inp.remove(btA);
            JOptionPane.showMessageDialog(null, "deleted connection ID: " + btA.id + " to ID: " + btB.id);
        }else{                                  //* if not connected  -> connect
            btA.out.add(btB);
            btB.inp.add(btA);
            JOptionPane.showMessageDialog(null, "connected ID: " + btA.id + " to ID: " + btB.id);
        }
        //* update state of both bts
        btA.updateState();
        btB.updateState();
    }

    abstract public void updateState();  // abstract updateState method -> every gate defines his own logic

    public void updateFollower(){
        for(int i = 0; i < out.size(); i++){
            out.get(i).updateState();
        }
    }
    public void showMsg(String msg){
        JOptionPane.showMessageDialog(null, msg);
    }
}