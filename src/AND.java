/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CJA
 */
public class AND extends Bauteil{
    AND(int id){
        super();
        this.id = id;
        setBounds(this.btnX + ((this.id % 7) * this.btnShift), this.btnY + ((this.id % 7) * this.btnShift), this.btnWidtht, this.btnHeight);
        setText("AND " + this.id);
        System.out.println("Created AND id:" + id);
        updateState();
    }

    public void updateState(){
        int countTrue = 0;
        for(int i = 0; i < inp.size(); i++){
            if(inp.get(i).state == true) countTrue++;
        }
        if(countTrue == inp.size()) this.state = true;
        else this.state = false;
        updateColor();
        updateFollower();
    }

}
