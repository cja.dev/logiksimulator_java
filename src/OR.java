/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author CJA
 */
public class OR extends Bauteil{
    OR(int id){
        super();
        this.id = id;
        setBounds(this.btnX + ((this.id % 7) * this.btnShift), this.btnY + ((this.id % 7) * this.btnShift), this.btnWidtht, this.btnHeight);
        setText("OR " + this.id);
        System.out.println("Created OR id:" + id);
        updateState();
    }
    public void updateState(){
        int countTrue = 0;
        for(int i = 0; i < inp.size(); i++){
            if(inp.get(i).state == true) countTrue++;
        }
        if(countTrue > 0) this.state = true;
        else this.state = false;
        updateColor();
        updateFollower();
    }
}
